package com.example.changescene.demochangescene;

import com.example.changescene.demochangescene.controllers.GenericController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

public class ChangeSceneApplication extends Application {
    @Override
    public void start(Stage mainStage) throws IOException {
        // Creamos os cargadores de FXML para cada unha das Scene
        FXMLLoader mainLoader = new FXMLLoader(ChangeSceneApplication.class.getResource("mainView.fxml"));
        FXMLLoader formLoader = new FXMLLoader(ChangeSceneApplication.class.getResource("formView.fxml"));
        FXMLLoader otherLoader = new FXMLLoader(ChangeSceneApplication.class.getResource("view2.fxml"));

        // Habitualmente usamos directamente o loader.load() no constructor da escea
        // neste caso precisamos o obxecto Parent para obter por un lado o compoñente pai da vista
        // pero tamén precisamos obter o obxecto de tipo controller
        Parent mainPane = mainLoader.load();
        Parent formPane = formLoader.load();
        Parent otherPane = otherLoader.load();

        // Creamos as esceas
        Scene mainScene = new Scene(mainPane, 600, 600);
        Scene formScene = new Scene(formPane, 600, 600);
        Scene view2Scene = new Scene(otherPane, 600, 600);

        // Configuración do manexador de escenas agregando todas as esceas que temos na aplicación
        SceneHandler sch = new SceneHandler(mainStage);
        sch.addScene(SceneHandler.MAIN_SCENE, mainScene);
        sch.addScene(SceneHandler.FORM_SCENE, formScene);
        sch.addScene(SceneHandler.OTHER_SCENE, view2Scene);

        // Podemos rexistrar todos os Controllers tamén nun mapa, e logo agregamos en cada un deles o manexador para
        // que se poidan realizar as operacións de cambiar de escea dende eles ao lanzar eventos
        HashMap<String, GenericController> controllers = new HashMap<>();
        controllers.put(SceneHandler.MAIN_SCENE, mainLoader.getController());
        controllers.put(SceneHandler.FORM_SCENE, formLoader.getController());
        controllers.put(SceneHandler.OTHER_SCENE, otherLoader.getController());
        controllers.values().stream().forEach(controller -> controller.setSch(sch));

        mainStage.setTitle("Change Scene Example!");
        mainStage.setScene(mainScene);
        mainStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
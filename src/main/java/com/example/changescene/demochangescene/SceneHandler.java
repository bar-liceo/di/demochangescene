package com.example.changescene.demochangescene;

import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.HashMap;

public class SceneHandler {
    // Estas son unhas constantes de clase cos nomes das esceas para non confundirse
    // Podiamos ter feito tamén un enumerado
    public static final String  MAIN_SCENE = "mainScene";
    public static final String  FORM_SCENE = "formScene";
    public static final String  OTHER_SCENE = "view2Scene";

    // Neste HashMap é onde se rexistran as esceas e poden accederse a elas mediante o nome de escea
    private HashMap<String, Scene> scenes;
    // Necesitamos ter a Stage, porque á hora de cambiar a escena temos que facelo vía un obxecto deste tipo.
    private Stage stage;

    public SceneHandler(Stage stage) {
        // o único que facemos ao instanciar StageHandler é inicializar o HashMap e asignar o Stage
        this.scenes = new HashMap<>();
        this.stage = stage;
    }

    public void addScene(String sceneName, Scene scene){
        // O que se fai é rexistrar a Scene co seu nome correspondente no HashMap
        this.scenes.put(sceneName, scene);
    }

    public void changeToScene(String sceneName){
        // Para cambiar a Scene o que fai falla é facer uso do método setScene da Stage
        this.stage.setScene(this.scenes.get(sceneName));
    }

}

package com.example.changescene.demochangescene.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

// como implementamos as cousas genericas de todos os controllers do noso programa
// non temos que facer nada en cada un deles salvo que extendan do xenérico
public class FormController extends GenericController {
    @FXML
    private Label labelHello;
    @FXML
    private TextField tfName;
    @FXML
    private TextField tfSurname;
    @FXML
    private TextField tfAge;

    @FXML
    protected void onSendButtonClick() {
        labelHello.setText("Ola " + tfName.getText() + " " + tfSurname.getText() + ", que tal levas os " + tfAge.getText() + " anos?");
    }


}
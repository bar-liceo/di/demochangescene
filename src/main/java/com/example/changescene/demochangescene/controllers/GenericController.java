package com.example.changescene.demochangescene.controllers;

import com.example.changescene.demochangescene.SceneHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class GenericController {

    // Todos os controllers do noso programa teñen que ter un obxecto manexador
    protected SceneHandler sch;
    public void setSch(SceneHandler sch) {
        this.sch = sch;
    }

    // En lugar de usar en cada controller a súa función
    // temos no controller xenérico unha función para xestionar
    // os eventos, deste xeito teñen que chamarse no FXML igual en todas as vistas
    @FXML
    public void onOtherSceneButtonClick(ActionEvent actionEvent) {
        this.sch.changeToScene(SceneHandler.OTHER_SCENE);
    }
    @FXML
    public void onMainSceneButtonClick(ActionEvent actionEvent) {
        this.sch.changeToScene(SceneHandler.MAIN_SCENE);
    }
    @FXML
    public void onFormularioButtonClick(ActionEvent actionEvent) {
        this.sch.changeToScene(SceneHandler.FORM_SCENE);
    }
}

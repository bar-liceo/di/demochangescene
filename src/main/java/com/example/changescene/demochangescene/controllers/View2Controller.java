package com.example.changescene.demochangescene.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

// como implementamos as cousas genericas de todos os controllers do noso programa
// non temos que facer nada en cada un deles salvo que extendan do xenérico
public class View2Controller extends GenericController {
    @FXML
    private Label welcomeText2;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText2.setText("Welcome to JavaFX Application!");
    }

}
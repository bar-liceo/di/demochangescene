package com.example.changescene.demochangescene.controllers;

import com.example.changescene.demochangescene.SceneHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

// como implementamos as cousas genericas de todos os controllers do noso programa
// non temos que facer nada en cada un deles salvo que extendan do xenérico
public class ChangeSceneMainController extends GenericController {
    @FXML
    private Label labelView;

    @FXML
    public void onFormularioButtonClick(ActionEvent actionEvent) {
        this.sch.changeToScene(SceneHandler.FORM_SCENE);
    }

    @FXML
    public void onView2ButtonClick(ActionEvent actionEvent) {
        this.sch.changeToScene(SceneHandler.OTHER_SCENE);
    }
}
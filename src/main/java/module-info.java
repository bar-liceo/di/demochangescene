module com.example.changescene.demochangescene {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.changescene.demochangescene to javafx.fxml;
    exports com.example.changescene.demochangescene;
    exports com.example.changescene.demochangescene.controllers;
    opens com.example.changescene.demochangescene.controllers to javafx.fxml;
}